package com.superman;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
/**
 * 
 * @author jeev567
 * @Description This is a sample project for quick reference for REST API
 * Please note that we need add a web descriptor and mention the rest related servlet
 * to make the project up and running 
 */
@Path("/hello")
public class Hello {
	
	@GET
	@Produces(MediaType.TEXT_XML)
	public String sayHello() {
		String response = "<?xml version='1.0'?>"
				+ "<hello>This is from XML</hello>";
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String sayHelloJSON() {
		String response = null;
		return response;
	}

	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHelloHTML(@QueryParam("name") String name,
			@QueryParam("Card_no") String Card_no,
			@QueryParam("amount") int amount) {
		String response = "<h1>This is HTML Response </h1>";

		if (amount > 100000) {
			response = "Credit Card is not approved";
		} else {

			response = "Credit Card is approved";
		}

		return "<html><title>Credit Card Authorization</title>" + "<body><h1>"
				+ response + "</h1></body>" + "</html>";
	}

}
